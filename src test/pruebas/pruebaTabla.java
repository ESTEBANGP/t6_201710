package pruebas;

import Estructures.EncadenamientoSeparadoTH;
import junit.framework.TestCase;

public class pruebaTabla extends TestCase {

	
	private EncadenamientoSeparadoTH<String, Integer> tabla;
	
	private void setupEscenario1(){
		tabla = new EncadenamientoSeparadoTH<>(5);
		tabla.insertar("Esteban", 10);
		tabla.insertar("Alejandro", 9);
		tabla.insertar("Esteban", null);
		tabla.insertar("Teresa", 8);
		tabla.insertar("Teresa", 30);
	}
	
	public void testInsertar1(){
		
		setupEscenario1();
		
		assertNull(tabla.darValor("Esteban"));
		int comparar2 = tabla.darValor("Teresa");
		assertEquals(30,comparar2 );
		int comparar3 = tabla.darValor("Alejandro");
		assertEquals(9, comparar3);

	}
	
	
}
