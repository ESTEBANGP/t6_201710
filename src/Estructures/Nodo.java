package Estructures;


public class Nodo <K extends Comparable<K>, V>
{
	private Nodo<K,V> next;
	
	private Nodo<K, V> back;

	private K llave;
	
	private V valor;

	public Nodo (K key, V value)
	{
		next = null;
		back = null;
		llave = key;
		valor = value;
	}

	public void cambiarSiguiente(Nodo<K,V> siguiente)
	{
		this.next = siguiente;
	}

	public Nodo<K,V> darNext()
	{
		return next;
	}
	
	public void cambiarAnterior(Nodo<K,V> anterior)
	{
		this.back=anterior;
	}

	public Nodo<K,V> darBack()
	{
		return back;
	}

	public void cambiarValor(V nuevo)
	{
		this.valor = nuevo;
	}
	
	public void cambiarLlave(K llave){
		this.llave=llave;
	}

	public V darValor()
	{
		return valor;
	}
	public K darLlave(){
		return llave;
	}
}
