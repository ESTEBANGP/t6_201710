package Estructures;

public class EncadenamientoSeparadoTH <K extends Comparable<K>, V> {

	private ListaLlaveValorSecuencial<K, V> [] tabla;

	private int tama�o;

	public EncadenamientoSeparadoTH (int m){
		
		tama�o = m;
		tabla = new ListaLlaveValorSecuencial [m];

		for (int i = 0; i < m; i++) {
			tabla [i] = new ListaLlaveValorSecuencial<>();
		}
	}

	public int darTamanioTabla ()
	{
		return tama�o;
	}
	
	public int darTamanio(){
		int contador=0;
		for (int i = 0; i < tabla.length; i++) {
			contador += tabla[i].darTama�o();
		}
		
		return contador;
	}

	public boolean estaVacia()
	{
		if (tama�o <= 0)
		{
			return true;
		}
		return false;
	}

	public boolean tieneLlave(K llave)
	{
		boolean tiene = false;
		if (!estaVacia())
		{
			for (int i = 0; i < tabla.length && tiene==false; i++) 
			{
				ListaLlaveValorSecuencial<K, V> actual = tabla[i];
				if (actual.tieneLlave(llave) == true)
				{
					tiene = true;
				}
			}
		}
		return tiene;
	}

	public V darValor(K llave)
	{
		
		if (tieneLlave(llave))
		{
			for (int i = 0; i < tabla.length; i++) {

				ListaLlaveValorSecuencial<K, V> actual = tabla[i];

				V valorA = actual.darValor(llave);
				if (valorA!=null){
					return valorA;
				}
			}
		}
		return null;
	}

	public void insertar(K llave, V valor)
	{
			rehash();
			int posicion = hash(llave);
			ListaLlaveValorSecuencial<K, V> listaAgregar = tabla [posicion];
			listaAgregar.insertar(llave, valor);

	
	}
	
	private void rehash (){

		if ((darTamanio()/tama�o)>=8){
			tama�o=tama�o*2;
			ListaLlaveValorSecuencial<K, V> [] referencia = tabla;
			tabla = new ListaLlaveValorSecuencial [tama�o];
			
			for (int i = 0; i < tama�o; i++) {
				tabla[i] = new ListaLlaveValorSecuencial<>();
			}
			
			for (int i = 0; i < referencia.length; i++) {
				tabla [i] = referencia  [i];
			}
		}
	}
	
	public Iterable<K> llaves(){
		return tabla[0].llaves();
	}
	
	private int hash( K llave)
	{
		return (llave.hashCode() & 0x7fffffff)%tama�o;
	}

	public int [] darLongitudListas(){
		int [] arregloTama�o = new int [tama�o];
		for (int i = 0; i < tabla.length; i++) {
			arregloTama�o[i]= tabla[i].darTama�o();
		}
		return arregloTama�o;
	}









}
