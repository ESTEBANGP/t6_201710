package Estructures;

public class ListaLlaveValorSecuencial<K extends Comparable <K>,V>
{

	private Nodo<K, V> primero;
	private Nodo<K, V> actual;
	private Nodo<K, V> ultimo;
	private int tamaņoLista;

	public ListaLlaveValorSecuencial()
	{
		primero=null;
		tamaņoLista = 0;
	}

	public int darTamaņo(){
		return tamaņoLista;
	}

	public boolean estaVacia(){
		if (tamaņoLista<=0) {
			return true;
		}
		return false;
	}

	public boolean tieneLlave(K llave){
		Nodo<K, V> cambia= primero;
		if (tamaņoLista>0){
			while (cambia != null) {

				if (cambia.darLlave().equals(llave)){
					return true;
				}
				cambia=cambia.darNext();
			}
		}
		return false;
	}
	
	public Nodo<K, V> darNodoLlave (K llave){
		Nodo<K, V> cambia= primero;
		if (tamaņoLista>0){
			while (cambia != null) {

				if (cambia.darLlave().equals(llave)){
					return cambia;
				}
				cambia=cambia.darNext();
			}
		}
		return null;
	}

	public V darValor(K llave) {

		Nodo<K, V> cambia= primero;
		if (tamaņoLista>0){
			while (cambia != null) {

				if (cambia.darLlave().equals(llave)){

					return cambia.darValor();
				}
				cambia=cambia.darNext();
			}
		}

		return null;
	}

	public void insertar(K llave, V valor){
		if (tieneLlave(llave) == false && valor != null) {
			tamaņoLista++;
			Nodo<K, V> agregar = new Nodo<K, V>(llave, valor);
			if(primero==null){
				primero= agregar;
				ultimo= primero;

			}
			else{
				Nodo<K, V> cambiando = agregar;
				cambiando.cambiarAnterior(ultimo);
				Nodo<K, V> anterior = cambiando.darBack();
				int contador = 0;
				
				while (anterior !=null && anterior.darLlave().compareTo(cambiando.darLlave())>0) {
					K elementoReferencia = cambiando.darLlave();
					V elementoReferenciaV = cambiando.darValor();
					cambiando.cambiarValor(anterior.darValor());
					cambiando.cambiarLlave(anterior.darLlave());
					anterior.cambiarValor(elementoReferenciaV);
					anterior.cambiarLlave(elementoReferencia);
					
					if (contador==0){
						ultimo.cambiarSiguiente(cambiando);
						ultimo = cambiando;
					}

					
					cambiando = anterior;
					anterior = anterior.darBack();

					contador ++;
				}

				
			}
			
		}
		else if (tieneLlave(llave)== true && valor !=null ){
			Nodo<K, V> buscado = darNodoLlave(llave);
			buscado.cambiarValor(valor);
		}
		else if (tieneLlave(llave)==true && valor ==null){
			Nodo<K, V> buscado = darNodoLlave(llave);
			if (buscado.equals(primero)){
				primero= buscado.darNext();
			}
			else if (buscado.equals(ultimo)){
				buscado.darBack().cambiarSiguiente(null);
				ultimo= buscado.darBack();
				
			}
			else{
				Nodo<K, V> referencia = buscado.darBack();
				referencia.cambiarSiguiente(buscado.darNext());
				buscado.darNext().cambiarAnterior(referencia);
			}
		}
	}
	
	public  Iterable <K> llaves(){
		return (Iterable<K>) new IteradorListaLlave(primero);
	}
	
	



}
