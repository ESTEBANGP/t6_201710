package Estructures;

import java.util.Iterator;


public class IteradorListaLlave <K extends Comparable <K>,V>  implements Iterator<K> {

	private Nodo<K, V> actual;
	
	public IteradorListaLlave(Nodo<K, V> pNodo){
		actual = pNodo;
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		if (actual!=null){
			return false;
		}
		return true;
	}

	@Override
	public K next() {
		// TODO Auto-generated method stub
		Nodo<K, V> referencia = actual;
		K item = referencia.darLlave();
		actual= actual.darNext();
		return item;
	}
	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}

}
