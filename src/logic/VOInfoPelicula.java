package logic;

public class VOInfoPelicula 
{

	private String titulo;
	private int anio;
	private String director;
	private String actores;
	private double ratingIMDB;
	
	
	public String darTitulo()
	{
		return titulo;
	}
	public void setTitulo(String pTitulo)
	{
		this.titulo = pTitulo;
	}
	
	public int darAnio()
	{
		return anio;
	}
	public void setAnio(int pAnio)
	{
		this.anio = pAnio;
	}
	
	public String darDirector()
	{
		return director;
	}
	public void setDirector(String pDirector)
	{
		this.director = pDirector;
	}
	
	public String darActores()
	{
		return actores;
	}
	public void setActores(String pActores)
	{
		this.actores = pActores;
	}
	
	public double darRating()
	{
		return ratingIMDB;
	}
	public void setRating(double pRating)
	{
		this.ratingIMDB = pRating;
	}
	
}
