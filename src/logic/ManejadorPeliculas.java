package logic;
import java.io.FileReader;

import Estructures.EncadenamientoSeparadoTH;

import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Iterator;
public class ManejadorPeliculas 
{

	private VOInfoPelicula datos;
	private EncadenamientoSeparadoTH<String, String> tabla;

	public static void main(String args[]) throws java.io.IOException {
		JsonParser parser = new JsonParser();
		BufferedReader br = new BufferedReader(new FileReader("./data/links_json.json"));
		JsonElement datos = parser.parse(br);
		long tiempoInicial = System.currentTimeMillis();
		cargarDatos(datos);
		long tiempoFinal = System.currentTimeMillis();
		long final1= (tiempoFinal-tiempoInicial);
		System.out.println("El tiempo que demoro en cargar fue: "+ final1 );
	}

	public static void cargarDatos(JsonElement elemento)
	{
		VOInfoPelicula datos2 = null;
		String idPelicula = null;
		EncadenamientoSeparadoTH<String, String> tabla = null;
		if (elemento.isJsonObject()) {
			JsonObject obj = elemento.getAsJsonObject();
			java.util.Set<java.util.Map.Entry<String,JsonElement>> entradas = obj.entrySet();
			java.util.Iterator<java.util.Map.Entry<String,JsonElement>> iter = entradas.iterator();
			while (iter.hasNext()) {
				java.util.Map.Entry<String,JsonElement> entrada = iter.next();
				
				System.out.println("dato: " + entrada.getKey());
				cargarDatos(entrada.getValue());
			}

		} else if (elemento.isJsonArray()) {
			JsonArray array = elemento.getAsJsonArray();
			java.util.Iterator<JsonElement> iter = array.iterator();
			while (iter.hasNext()) {
				JsonElement entrada = iter.next();
				cargarDatos(entrada);
			}
		} else if (elemento.isJsonPrimitive()) {
			JsonPrimitive valor = elemento.getAsJsonPrimitive();       

			if (valor.isString()) 
			{
				System.out.println("valor: " + valor.getAsString());
			}
		} else if (elemento.isJsonNull()) {
			System.out.println("Es NULL");
		} else {
			System.out.println("Es otra cosa");
		}
	}

}
