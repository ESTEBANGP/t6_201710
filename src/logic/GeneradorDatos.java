package logic;

import Estructures.EncadenamientoSeparadoTH;

public class GeneradorDatos {

	public String[] generarCadenas ( int N, int k){
		String [] cFinal = new String [N+1]; 
		int numRandon = (int) Math.round(Math.random() * k-1);

		String palabraAgregar = "a";
		
		for (int i = 0; i < cFinal.length; i++) {

			for (int j = 0; j < numRandon; j++) {
				palabraAgregar+="a";
			}
			cFinal[i] = palabraAgregar;
		
		}

		return cFinal;
	}

	public Integer [] generarNumeros (int N){

		Integer [] nFinal = new Integer [N+1]; 

		for (int i=0; i<N; i++){
			int numRandon = (int) Math.round(Math.random() * 1999);
			nFinal [i] = numRandon;

		}


		return nFinal;
	}
	
	public static void main(String[] args) {
		int max=5000;
		int mus= 100;
		EncadenamientoSeparadoTH<String, Integer> tabla = new EncadenamientoSeparadoTH<>(max);
		GeneradorDatos generador = new GeneradorDatos();
		String [] llaves = generador.generarCadenas(max,mus);
		Integer [] valores = generador.generarNumeros(max);
		
		long tiempoInicial = System.currentTimeMillis();	
		
		for (int i = 0; i < valores.length && i<llaves.length; i++) {
			String llave = llaves[i];
			Integer valor = valores[i];
			tabla.insertar(llave, valor);
		}
		
		long tiempoFinal = System.currentTimeMillis();
		
		long final1= (tiempoFinal-tiempoInicial);
		
		System.out.println("El tiempo que demoro en insertar a la tabla fue: "+ final1 );
	}


}
